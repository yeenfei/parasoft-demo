package com.parasoft.examples.controller;

import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.parasoft.examples.model.Cat;
import com.parasoft.examples.model.Dog;
import com.parasoft.examples.model.Pet;
import com.parasoft.examples.service.PetService;

/**
 * An example controller with a Bean name, which manages Pet objects.
 */
@Controller("NamedPet")
public class NamedPetController {

    // The PetService, which Spring will inject based on the Bean name
    @Resource
    @Qualifier("PetService")
    private PetService petService;

    /**
     * An MVC handler method which retrieves a Pet by name.
     * @param name
     * @return ResponseEntity
     */
    @RequestMapping("/pets/{name}")
    public ResponseEntity<Pet> getByName(@PathVariable("name") String name)
    {
        Pet pet = petService.find(name);
        if (pet == null) {
            return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Pet>(pet, HttpStatus.OK);
    }

    /**
     * An MVC handler method which retrieves all pets by type
     * @param name either 'cat' or 'dog'
     * @return ResponseEntity
     */
    @RequestMapping(path="/pets")
    public ResponseEntity<Collection<? extends Pet>> getByType(@RequestParam("type") String type)
    {
        Collection<? extends Pet> pets = petService.findByType(type.equals("cat") ? Cat.class : Dog.class);
        return new ResponseEntity<Collection<? extends Pet>>(pets, HttpStatus.OK);
    }
}
