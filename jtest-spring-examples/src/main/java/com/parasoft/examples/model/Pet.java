package com.parasoft.examples.model;

/**
 * An example abstract class which other POJOs can extend
 */
public abstract class Pet {
    private String name;

    protected Pet(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public abstract void speak();
}
