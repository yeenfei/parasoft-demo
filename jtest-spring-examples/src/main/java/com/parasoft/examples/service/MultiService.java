
package com.parasoft.examples.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.parasoft.examples.model.Person;
import com.parasoft.examples.model.Pet;

/**
 * An example Service with multiple dependencies.
 */
public class MultiService {

    // An autowired dependency
    @Autowired
    private PersonService personServ;

    // A Constructor-provided dependency
    private PetService petServ;

    public MultiService(PetService petService)
    {
        petServ = petService;
    }

    public Person getPerson(String name)
    {
        return personServ.findPerson(name);
    }

    public Pet getPet(String name)
    {
        return petServ.find(name);
    }
}
