
package com.parasoft.examples.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.parasoft.examples.model.Cat;
import com.parasoft.examples.model.Dog;
import com.parasoft.examples.model.Pet;

/**
 * An example Service which manages Pet objects.<br/>
 * Default values are added when this service is constructed.
 */
public class PetService {
    private Map<Integer, Pet> pets = new HashMap<Integer, Pet>();

    public PetService()
    {
        pets.put(1, new Dog("Spot"));
        pets.put(2, new Cat("Fluffy"));
    }

    public Pet getPet(int id)
    {
        return pets.get(id);
    }

    public Collection<Pet> getAllPets()
    {
        return pets.values();
    }

    public Pet find(String name)
    {
        for (Pet pet : pets.values()) {
            if (pet.getName().equals(name)) {
                return pet;
            }
        }
        return null;
    }

    public <T> Collection<T> findByType(Class<T> type)
    {
        List<T> ret = new ArrayList<T>();
        for (Pet pet : pets.values()) {
            if (type.isInstance(pet)) {
                ret.add(type.cast(pet));
            }
        }
        return ret;
    }
}
