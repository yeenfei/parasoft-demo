package com.parasoft.examples.service;

import java.util.Collection;

import com.parasoft.examples.domain.Person;

public interface IPersonService
{
    Person getPerson(int id);

    Collection<Person> getAllPeople();

    Person findPerson(String name);
}
