package com.parasoft.examples.web;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.ui.Model;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.parasoft.examples.domain.Person;
import com.parasoft.examples.service.PersonService;

/**
 * An example Junit test for PeopleController with populated values and assertions
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PeopleControllerTest {

    // Controller under test
    @Autowired
    PeopleController controller;

    // Parasoft Jtest UTA: Spring MVC test support class
    @Autowired
    MockMvc mockMvc;

    // Parasoft Jtest UTA: Configure test dependencies using Java-based configuration
    @EnableWebMvc
    @Configuration
    static class Config {
        /**
         * Parasoft Jtest UTA: Controller under test
         *
         * @see com.parasoft.examples.controller.PeopleController
         */
        @Bean
        public PeopleController getPersonController() {
            return new PeopleController();
        }

        /**
         * Parasoft Jtest UTA: Dependency generated for autowired field
         * "personService" in AbstractPeopleController
         *
         * @see com.parasoft.examples.controller.AbstractPeopleController#personService
         */
        @Bean
        public PersonService getPersonService() {
            PersonService personService = mock(PersonService.class);
            when(personService.getPerson(1)).thenReturn(new Person("John", 32));
            return personService;
        }
    }

    // Parasoft Jtest UTA: Initialize Spring MVC test support class
    @Before
    public void setup() {
        
    }

    /**
     * Parasoft Jtest UTA: Test for getPerson(int)
     *
     * @see com.parasoft.examples.controller.PeopleController#getPerson(int)
     */
    @Test
    public void testGetPerson() throws Exception {
        // When
        int id = 1;
        ResultActions actions = mockMvc.perform(get("/people/" + id));

        // Then
        actions.andExpect(status().isOk());
        actions.andExpect(content().string("{\"name\":\"John\",\"age\":32}"));
    }

    /**
     * Parasoft Jtest UTA: Test for people(Model)
     *
     * @see com.parasoft.examples.controller.PeopleController#people(Model)
     */
    @Test
    public void testPeople() throws Exception {
        // When
        ResultActions actions = mockMvc.perform(get("/people"));

        // Then
        actions.andExpect(status().isOk());
        actions.andExpect(view().name("people.jsp"));
    }
}
